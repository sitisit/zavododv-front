import { Store } from 'vuex'

declare module 'vuex/types/index' {
  interface Store<any> {
    $cookies: any
    $api: any
  }
}
