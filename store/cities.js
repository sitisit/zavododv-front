export const state = () => ({
    cities: [],
})

export const mutations = {
    setCities(state, cities) {
        state.cities = cities;
    }
}

export const actions = {
    async getCities({ commit, dispatch, state }) {

        if (state.cities.length) {
            return
        }

        const cities = await this.$api.agent.transport
            .get(`stores/cities`)
            .then((res) => {
                return res.data;
            })
            .catch((err) => {
                console.log(err?.request?.data?.[0]?.message) || "Ошибка сервера";
            });

        commit('setCities', cities);

    }

}
