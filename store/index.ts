/* eslint-disable max-len */
import axios from 'axios';
import { BASE_API_URL, CONF_TOKEN } from '~/nuxt.config';

export const state = () => ({});

export const actions = {
  /**
   * Получение конфигурации сайта
   */
  async nuxtServerInit(store: any): Promise<void> {
    try {
      const instance = axios.create({
        baseURL: BASE_API_URL,
        headers: { Authorization: `Bearer ${CONF_TOKEN}` },
      });

      const res = await instance.get('configs');

      store.commit('appState/setConfig', res.data, { root: true });
    } catch (e) {
      console.error('Невозможно получить конфигурацию ', e);
    }
  },
};
