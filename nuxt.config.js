export const BASE_API_URL = "https://odv.sitisit.ru/api/";
export const CONF_TOKEN =
  "a26ac89e14e68c5f8f1e1246622b88ec83774411009035bbd95e614acebf9f553EuOp-e0m40lWWyqx8T7ZW6ToSLUFnVf";
export default {
  server: {
    port: 3000,
    host: "0.0.0.0",
  },
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: "Завод рецептурной оптики ОДВ",
    htmlAttrs: {
      lang: "en",
    },
    meta: [
      { charset: "utf-8" },
      {
        name: "viewport",
        content: "width=device-width, initial-scale=1 user-scalable=no",
      },
      { hid: "description", name: "description", content: "" },
    ],
    link: [{ rel: "icon", type: "image/x-icon", href: "/favicon.ico" }],
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: ["~/assets/scss/main.scss"],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    // '~plugins/vue-awesome-swiper.js',
    "~/plugins/Vuelidate.js",
    "~plugins/vue-js-modal.js",
    "~plugins/vue-mask.js",
    "~plugins/vue-cool-lightbox.js",
    "~plugins/vue-awesome-swiper.js",
  ],

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/typescript
    "@nuxt/typescript-build",
    // https://go.nuxtjs.dev/tailwindcss
    "@nuxt/postcss8",
  ],
  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    "cookie-universal-nuxt",
    "@nuxt/image",
    "@nuxtjs/style-resources",
    "@nuxtjs/toast",

    [
      "~/node_modules/sitis-nuxt/lib",
      {
        baseUrl: BASE_API_URL,
        // cmsRoot: 'cms' - default
      },
    ],
  ],
  image: {
    providers: {
      imageProvider: {
        name: "imageProvider",
        provider: `${__dirname}/providers/imageProvider`,
        options: {
          modifiers: {
            format: "jpg",
            // quality: 100
          },
          baseURL: BASE_API_URL + "storage/image/",
        },
      },
    },
    provider: "imageProvider",
  },
  // '@nuxtjs/toast' config
  toast: {
    position: "top-right",
    duration: 3000,
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    postcss: {
      plugins: {
        tailwindcss: {},
        autoprefixer: {},
      },
    },
  },
};
