// const defaultTheme = require('tailwindcss/defaultTheme')

module.exports = {
  content: [
    './components/**/*.{vue,js}',
    './layouts/**/*.vue',
    './pages/**/*.vue',
    './plugins/**/*.{js,ts}',
    './components/**/*.{vue,js}',
    './nuxt.config.{js,ts}',
    './cms/**/*.vue',

  ],

  theme: {

    fontFamily: {
      'sans': ['GothamPro'],
      'GothamPro': ['GothamPro'],
      'Intro': ['Intro'],
      'ProximaNova': ['ProximaNova'],

    },
    extend: {
      colors: {
        primary: '#a5188b',
        secondary: '#ffd453',
      },
      screens: {
        '2xs': '319px',
        'xs': '480px',
        'sm': '640px',
        'md': '800px',
        'lg': '1000px',
        'xl': '1310px',
        '2xl': '1536px',

      }
    },
  },
}
